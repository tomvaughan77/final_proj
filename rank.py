

class Ranker:
    import random
    import os
    import numpy as np
    import matplotlib.pyplot as plt
    import datetime
    import math
    from sentence import Sentence
    global random, os, np, plt, datetime, math, Sentence

    def __init__(self, data, path=False, percent=10):
        def importFromFile(file):
            s = []
            count = -3

            with open(path, 'rb') as f:
                for line in f:
                    l = line.strip()
                    if (l == ""):
                        print "BLANK"
                    else:
                        print l
                        if (count == -3):
                            print "Importing table from save at: " + str(l)
                        elif (count == -2):
                            self.t_percent = int(l)
                        elif (count == -1):
                            self.t_init_size = int(l)
                        elif (count == 0 or count == 3):
                            s.append(int(l))
                        elif (count == 2 or count == 4):
                            s.append(float(l))
                        elif (count == 1):
                            s.append(str(l).split())

                        if (count == 4):
                            count = 0
                            self.t_max_index += 1
                            self.table.append(Sentence(s[0], s[1], s[2], s[3], s[4]))
                            self.t_plays += s[3]
                            s = []
                        else:
                            count += 1
                print "EOF\n"

                self.t_portion_size = int(math.ceil(float(self.t_init_size) / float(self.t_percent)))
                self.t_threshhold = int(math.ceil(float(self.t_init_size) * float(self.t_percent)))
                self.f_index = 0
                self.t_plays = int(round(float(self.t_plays) / 2.0))

                if self.__shouldWePrune() == True:
                    self.__prune()

        def addAllToTable(data):
            for i in range(0, len(data)):
                self.t_max_index += 1
                self.table.append(Sentence(self.t_max_index, data[i]))

        self.table = []
        self.t_max_index = -1
        self.t_plays = 0
        self.f_base_name = "output/table"

        #Are we importing from a file or not?
        if path == False:
            self.t_percent = percent
            self.f_index = 0
            self.t_init_size = len(data)
            self.t_portion_size = int(math.ceil(float(self.t_init_size) / float(self.t_percent)))
            self.t_threshhold = int(math.ceil(float(self.t_init_size) * float(self.t_percent)))

            addAllToTable(data)
        else: #Ignore what's in the data field entierly
            importFromFile(path)

        if self.t_portion_size < 5:
            raise Exception("Error - End size of table too small to be pruned on")

        self.printTable()

    def getData(self, ID):
        for i in range(0, len(self.table)):
            if self.table[i].getID() == ID:
                return self.table[i].getData()

    def exportToFile(self):
        f = open((str(self.f_base_name) + str(self.f_index) + ".txt"), 'w')
        self.f_index += 1
        f.write(str(datetime.datetime.now()) + "\n")
        f.write(str(self.t_percent) + "\n")
        f.write(str(self.t_init_size) + "\n\n")
        for i in self.table:
            f.write(str(i.getID()) + "\n")
            for j in range(0, 4):
                f.write(str(i.getData()[j]) + " ")
            f.write("\n")
            f.write(str(i.getRank()) + "\n")
            f.write(str(i.getComp()) + "\n")
            f.write(str(i.getCluster()) + "\n\n")
        f.close()

    def __shouldWePrune(self):
        #Determine how many prunes have happened so far
        #Determine how many prunes should have happened at this point
        #Return True if a prune is needed, False otherwise

        if (self.t_plays < self.t_threshhold) or (len(self.table) <= self.t_portion_size):
            return False

        prunes = math.floor((float(self.t_init_size) - float(len(self.table))) / float(self.t_portion_size))
        print "Prunes so far: " + str(prunes)

        correct = math.floor(float(self.t_plays) / float(self.t_threshhold))
        print "Correct no prunes: " + str(correct)

        if prunes != correct and prunes < correct:
            return True
        else:
            return False

    def printTable(self):
        print "================================"
        print "ID == Data == Rank == Comp == Cluster == Recent"
        print ""
        for i in self.table:
            print i.getID(), i.getData(), i.getRank(), i.getComp(), i.getCluster(), i.getRecent()

        print ""
        print "================================"
        print "Total elems in table   = ", len(self.table)
        print "Highest index in table = ", self.t_max_index
        print "Total plays of elems   = ", self.t_plays
        print "Total tests completed  = ", self.f_index
        print "Initial size of table  = ", self.t_init_size
        print "Final size of table    = ", self.t_portion_size
        print "Percentage             = ", self.t_percent
        print "Selection threshhold   = ", self.t_threshhold
        print "================================\n"

    def updateNFromComparison(self, array):
        def updateFromComparison(ID1, ID2):
            rank1 = float("-inf")
            rank2 = float("-inf")

            for i in range(0, len(self.table)):
                if (ID1 == self.table[i].getID()):
                    self.table[i].incComp()
                    rank1 = self.table[i].getRank()
                    break

            for i in range(0, len(self.table)):
                if (ID2 == self.table[i].getID()):
                    self.table[i].incComp()
                    rank2 = self.table[i].getRank()
                    break

            if rank1 != float("-inf") and rank2 != float("-inf"):
                qa = pow(10, rank1 / float(400))
                qb = pow(10, rank2 / float(400))

                ea1 = qa / (qa + qb)
                ea2 = qb / (qa + qb)

                rank1 = rank1 + (32 * (1 - ea1))
                rank2 = rank2 + (32 * (0 - ea2))

                for i in range(0, len(self.table)):
                    if (ID1 == self.table[i].getID()):
                        self.table[i].setRank(rank1)
                        break

                for i in range(0, len(self.table)):
                    if (ID2 == self.table[i].getID()):
                        self.table[i].setRank(rank2)
                        break

                self.t_plays += 1

                if self.__shouldWePrune() == True:
                    self.__prune()
            else:
                print "Either ID " + str(ID1) + " or ID " + str(ID2) + " don't exist"


        for i in array:
            #print "Updating " + str(i[0]) + " " + str(i[1])
            updateFromComparison(i[0], i[1])

    def pickPairs(self, n):
        def findNLeastPlayed():
            print "Simple pair choice"
            pairs = []

            for i in range(0, n):
                low_bound = float("inf")
                lows = []
                l1 = None
                l2 = None

                for j in range(0, len(self.table)):
                    if self.table[j].getComp() < low_bound:
                        low_bound = self.table[j].getComp()
                        del lows[:]
                        lows.append(j)
                    elif self.table[j].getComp() == low_bound:
                        lows.append(j)

                if len(lows) > 1:
                    r = random.randrange(0, len(lows), 1)
                    l1 = lows[r]
                    del lows[r]
                    r = random.randrange(0, len(lows), 1)
                    l2 = lows[r]
                elif len(lows) == 1:
                    l1 = lows[0]
                    del lows[:]
                    low_bound = float("inf")
                    for j in range(0, len(self.table)):
                        if self.table[j].getComp() != self.table[l1].getComp() and self.table[j].getComp() < low_bound:
                            low_bound = self.table[j].getComp()
                            del lows[:]
                            lows.append(j)
                        elif self.table[j].getComp() != self.table[l1].getComp() and self.table[j].getComp() == low_bound:
                            lows.append(j)
                    if len(lows) > 1:
                        r = random.randrange(0, len(lows), 1)
                        l2 = lows[r]
                    elif len(lows) == 1:
                        l2 = lows[0]
                    else:
                        raise Exception("Error - No 2nd least played row")
                else:
                    raise Exception("Error - No least played rows")

                self.table[l1].incComp()
                self.table[l2].incComp()
                self.table[l1].incRecent()
                self.table[l2].incRecent()
                pairs.append([l1, l2])

            for i in pairs:
                i[0] = self.table[i[0]].getID()
                i[1] = self.table[i[1]].getID()

            return pairs

        def findNRandomRanked():
            def addToList(list1, list2, pairs):
                count = 0
                while True:
                    l1 = random.choice(list1)
                    l2 = random.choice(list2)

                    check = True

                    if len(pairs) > 0:
                        '''for i in range(0, len(pairs)):
                            if (((self.table[l1].getID() == pairs[i][0]) and (self.table[l2].getID() == pairs[i][1])) or ((self.table[l2].getID() == pairs[i][0]) and (self.table[l1].getID() == pairs[i][1]))):
                                check = False
                                break'''

                        if l1 != l2 and check == True:
                            check = False
                            break

                        if (count >= 1000):
                            raise Exception("Error - Failed to break out of while loop while finding N random ranked")

                        count += 1
                    else:
                        if l1 != l2:
                            break

                pairs.append([self.table[l1].getID(), self.table[l2].getID()])
                self.table[l1].incRecent()
                self.table[l2].incRecent()
                self.table[l1].incComp()
                self.table[l2].incComp()
                return pairs

            print "Complex pair choice"
            pairs = []
            ranks = []
            for i in range(0, len(self.table)):
                ranks.append(self.table[i].getRank())

            ranks = np.sort(ranks)
            threshhold = np.mean(ranks) + np.std(ranks)
            high = [] #TODO These hold table positions, not IDs
            low = []

            for i in range(0, len(self.table)):
                if (self.table[i].getRank() >= threshhold):
                    high.append(i)
                else:
                    low.append(i)

            print "DEBUG"
            print high
            print "###########"
            print threshhold
            print len(high)
            print "\n\n"

            if len(high) < 2 or len(low) < 2:
                print "Warning: Number of sentences is too small to perform correct pair choice. Workaround being used"
                high = high + low
                low = low + high

            for i in range(0, n):
                r = random.randint(0, 3)
                if (r == 0):
                    #2 lows
                    #print "low low"
                    addToList(low, low, pairs)
                elif (r == 1):
                    # l1 low, l2 high
                    #print "low high"
                    addToList(low, high, pairs)
                elif (r == 2):
                    # l1 high, l2 low
                    #print "high low"
                    addToList(high, low, pairs)
                elif (r == 3):
                    # 2 highs
                    #print "high high"
                    addToList(high, high, pairs)
                else:
                    raise Exception("Dice incorrecltly configured")
            return pairs

        if self.t_plays < self.t_threshhold:
            pairs =  findNLeastPlayed()
        else:
            pairs = findNRandomRanked()
        return pairs

    def __prune(self):
        def getAllRanks():
            ranks = []
            for i in range(0, len(self.table)):
                ranks.append(self.table[i].getRank())
            return ranks

        def updateClusterScores():
            ranks = []
            for i in range(0, len(self.table)):
                ranks.append([self.table[i].getID(), self.table[i].getRank()])

            ranks = sorted(ranks, key=lambda x: x[1])

            for i in range(0, len(self.table)):
                c = 0
                if (i == 0):
                    c += abs(ranks[i][1] - ranks[i+1][1]) * 2.0
                    c += abs(ranks[i][1] - ranks[i+2][1])
                elif(i == 1):
                    c += abs(ranks[i][1] - ranks[i+2][1])
                    c += abs(ranks[i][1] - ranks[i+1][1])
                    c += abs(ranks[i][1] - ranks[i-1][1])
                elif(i == (len(ranks) - 1)):
                    c += abs(ranks[i][1] - ranks[i-1][1]) * 2.0
                    c += abs(ranks[i][1] - ranks[i-2][1])
                elif(i == len(ranks) - 2):
                    c += abs(ranks[i][1] - ranks[i+1][1])
                    c += abs(ranks[i][1] - ranks[i-1][1])
                    c += abs(ranks[i][1] - ranks[i-2][1])
                else:
                    c += abs(ranks[i][1] - ranks[i+2][1]) * 0.5
                    c += abs(ranks[i][1] - ranks[i+1][1])
                    c += abs(ranks[i][1] - ranks[i-1][1])
                    c += abs(ranks[i][1] - ranks[i-2][1]) * 0.5

                for j in range(0, len(self.table)):
                    if (self.table[j].getID() == ranks[i][0]):
                        self.table[j].setCluster(c)

        def removeFromTable(ID):
            for i in range(0, len(self.table)):
                if (ID == self.table[i].getID()):
                    del self.table[i]
                    if (self.t_max_index == ID):
                        highest = -1
                        for j in range(0, len(self.table)):
                            if (self.table[j].getID() > highest):
                                highest = self.table[j].getID()
                        self.t_max_index = highest
                    break

        print "\nPruning off " + str(self.t_portion_size) + " sentences..."

        for i in range(0, self.t_portion_size):
            updateClusterScores()

            ranks = getAllRanks()
            ranks = np.sort(ranks)
            stdev = np.mean(ranks) + np.std(ranks)
            lows = []

            for i in range(0, len(self.table)):
                if (self.table[i].getRank() <= stdev):
                    lows.append([self.table[i].getID(), self.table[i].getCluster()])

            r = random.randint(0, 3)
            if (r == 3):
                lowest = [-1, float("inf")]
                for j in range(0, len(self.table)):
                    if (self.table[j].getCluster() < lowest[1]):
                        lowest = [self.table[j].getID(), self.table[j].getCluster()]
                print "Removing id=" + str(lowest[0]) + " score=" + str(lowest[1]) + " (All)"
            else:
                lowest = [-1, float("inf")]
                for j in range(0, len(lows)):
                    if (lows[j][1] < lowest[1]):
                        lowest = [lows[j][0], lows[j][1]]

                for j in range(0, len(lows)):
                    if (lows[j][0] == lowest[0]):
                        del lows[j]
                        break

                print "Removing id=" + str(lowest[0]) + " score=" + str(lowest[1]) + " (Low)"
            removeFromTable(lowest[0])

    def updateRecents(self):
        print "Updating recents"
        for i in range(0, len(self.table)):
            if (self.table[i].getRecent() > 0):
                self.table[i].setComp(self.table[i].getComp() - self.table[i].getRecent())
                self.table[i].setRecent(0)
