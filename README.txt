To start:

screen
python app.py
ctrl-a, ctrl-d

To check on:

screen -r <PID OF CORRECT SCREEN>
*have a look at running script*
ctrl-a, ctrl-d

To stop:

screen -r <PID OF CORRECT SCREEN>
ctrl-c
ctrl-a, ctrl-d
killall screen

Snapshot of ranking table is saved in output/ directory after each user session and at startup

