class Sentence:

    def __init__(self, index, sentence, rank=1000, comp=0, cluster=0):
        self.ID = index
        self.data = sentence
        self.rank = float(rank)
        self.comp = int(comp)
        self.cluster = float(cluster)
        self.recent = 0

    def getID(self):
        return self.ID

    def getData(self):
        return self.data

    def getRank(self):
        return self.rank

    def getComp(self):
        return self.comp

    def getCluster(self):
        return self.cluster

    def getRecent(self):
        return self.recent

    def setRank(self, rank):
        self.rank = float(rank)

    def setComp(self, comp):
        self.comp = int(comp)

    def setCluster(self, cluster):
        self.cluster = float(cluster)

    def setRecent(self, recent):
        self.recent = int(recent)

    def incRecent(self):
        self.recent += 1

    def decRecent(self):
        self.recent -= 1

    def incComp(self):
        self.comp += 1

    def decComp(self):
        self.comp -= 1
