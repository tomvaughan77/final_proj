import numpy as np
import matplotlib.pyplot as plt
import sys
import random

global plays
plays = []
global no_sen
no_sen = []

def readIn(path):
    count = -3
    result = []
    s = []
    ranks = []

    with open(path, 'rb') as f:
        p = 0
        q = 0
        for line in f:
            l = line.strip()

            if (l != ""):
                if (count == 0):
                    s.append(int(l))
                elif (count == 2):
                    s.append(float(l))
                elif (count == 3):
                    p += int(l)
                    q += 1

                if (count == 4):
                    count = 0
                    ranks.append(s)
                    s = []
                else:
                    count += 1
    plays.append(p / 2)
    no_sen.append(q)
    return ranks

after = []

after.append(readIn("data/2/initial.txt"))
after.append(readIn("data/2/after20.txt"))
after.append(readIn("data/2/after60.txt"))
after.append(readIn("data/2/after110.txt"))
after.append(readIn("data/2/after160.txt"))
after.append(readIn("data/2/after260.txt"))

points = [0, 19, 56, 105, 154, 246]

#Take 48 random, plus highest and lowest ranked (50 total) fronm after160
ranks = []
highest = [-1, float("-inf")]
lowest = [-1, float("inf")]

for i in range(0, len(after[5])):
    if after[5][i][1] > highest[1]:
        highest = after[5][i]

    if after[5][i][1] < lowest[1]:
        lowest = after[5][i]

print "HIGHEST"
print highest
print "LOWEST"
print lowest

ranks.append(highest[0])
ranks.append(lowest[0])


random.shuffle(after[5])
counter = 0
limit = 48
while counter < limit:
    if after[5][counter][0] != highest[0] and after[5][counter][0] != lowest[0]:
        ranks.append(after[5][counter][0])
        counter += 1
    else:
        counter += 1
        limit += 1


for i in range(0, len(ranks)):
    ys = []
    for j in range(0, len(points)):
        #x = np.full(len(ranks), points[j])
        for k in range(0, len(after[j])):
            #print after[j][k][0]
            if ranks[i] == after[j][k][0]:
                ys.append(after[j][k][1])
                break
    plt.plot(points, ys)
plt.title("A graph showing how the ranks of 50 randomly sampled sentences\n change with the number of completed experiments\n")
plt.xlabel("Number of participant completed experiments")
plt.ylabel("Rank")
plt.show()



for i in range(0, len(points)):
    xs = []
    for j in range(0, len(after[i])):
        xs.append(after[i][j][1])

    xs.sort()
    axes = plt.gca()
    axes.set_ylim([0,100])
    axes.set_xlim([600,1600])
    plt.title("Distribution of ranks after " + str(plays[i]) + " comparisons over " + str(no_sen[i]) + " sentences\n")
    plt.xlabel("Rank")
    plt.ylabel("Frequency")
    plt.hist(xs, 'auto')
    plt.show()
